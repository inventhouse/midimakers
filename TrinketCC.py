# TrinketCC.py  --  Bank-switched MIDI CC controller for the Trinket M0
# Copyright (c) 2019 Benjamin Holt -- MIT License

from adafruit_dotstar import DotStar
from adafruit_midi import MIDI
from adafruit_midi.control_change import ControlChange
from analogio import AnalogIn
import board
import digitalio
import time
import usb_midi

###  Config  ###
# Number of banks * number of controls = total CCs; 4*4=16 CCs
# BANK_COLORS = ((255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 255))

# "lower white" and "upper white" are distinct banks (or sets of banks) to make 8*4=32 CCs
BANK_COLORS = ((255, 255, 255), (255, 0, 0), (255, 255, 0), (0, 255, 0), (0, 255, 255), (0, 0, 255), (255, 0, 255), (255, 255, 255))

SHADES = (1/4,)  # Just cut the brightness; swap in more shades to multiply the available banks
# SHADES = (1/8, 1/2)  # Double the number of banks
# SHADES = (1/8, 1/4, 1/2, 1)  # Quadruple the number of banks, 8 colors * 4 shades * 4 controls = 128 CCs!
BANK_COLORS = [ (int(r * s), int(g * s), int(b * s)) for (r,g,b) in BANK_COLORS for s in SHADES ]

SLEEP_DELAY = 1/100  # Seconds

FULL_RANGE = (0, 127)
KEYBOARD_RANGE = (36, 84)  # 49-note keyboard, C2 - B5
PIANO_RANGE = (21, 108)  # 88-note grand piano, A0 - C8

MIDI_CHANNEL = 1  # Set the MIDI channel (1-16)
midi = MIDI(midi_out=usb_midi.ports[1], out_channel=MIDI_CHANNEL - 1)  # MIDI channels are 1-indexed, out_channel is 0-indexed
bank_control = AnalogIn(board.D4)  # Analog input for bank selection
controls = [ AnalogIn(p) for p in (board.D3, board.D2, board.D1, board.D0) ]  # Analog controls, usually knobs or sliders
# control_ranges = {0: KEYBOARD_RANGE, 1: PIANO_RANGE,}  # Set ranges for CC outputs (0-indexed); remainder use full-range
control_ranges = {}  # Always use default full-range
dot = DotStar(board.APA102_SCK, board.APA102_MOSI, 1, brightness=0.1)
led = digitalio.DigitalInOut(board.D13)
led.direction = digitalio.Direction.OUTPUT

###  Utilities  ###
def map_range(x, in_min, in_max, out_min, out_max):
    mapped = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
    if out_min <= out_max:
        return max(min(mapped, out_max), out_min)
    return min(max(mapped, out_max), out_min)

###  Smoothing  ###
SMOOTH_A = 0.5  # The smoothing factor 𝛼, closer to 0 is smoother, closer to 1 is more responsive
SMOOTH_ITER = 4  # Extra iterations to keep updating the baseline as smoothing catches up to raw control
CONTROL_THRESHOLD = 256  # Threshold to update output; 256 = ~0.4% of 65k

###  State  ###
controls_base = [ c.value for c in controls ]  # Track the last "baseline" control readings that were mapped and sent
controls_smoothed = controls_base[:]  # Keep a simple exponentially-weighted average of each control reading
controls_iter = [0,] * len(controls)  # Extra iterations keep updating each control's baseline as smoothing catches up to raw control

while True:
    bank = int(map_range(bank_control.value, 0, 65535, 0, len(BANK_COLORS)))
    dot[0] = BANK_COLORS[bank]
    new_readings = [ c.value for c in controls ]
    controls_smoothed = [ s + SMOOTH_A * (n - s) for s, n in zip(controls_smoothed, new_readings) ]  # New smoothed control readings are the weighted average of the old smoothed and the new raw readings

    for i, v in enumerate(controls_smoothed):
        delta = v - controls_base[i]
        if abs(delta) >= CONTROL_THRESHOLD:  # If "significant" change, map and send
            controls_iter[i] = SMOOTH_ITER  # Set iterations for updating base
            control_num = i + bank * len(controls)
            lower, upper = control_ranges.get(control_num, FULL_RANGE)
            mapped_value = int(map_range(v, 0, 65535, lower, upper + 1))  # int() floors, so +1 to upper bound
            led.value = True  # Flash led when sending message
            midi.send(ControlChange(control_num, mapped_value))
            print("CC {}: {} - {} ({})".format(control_num, mapped_value, v, delta))
        if controls_iter[i] > 0:  # While control is changing and for a few iterations after, base should track the smoothed input
            controls_iter[i] -= 1
            controls_base[i] = v

    time.sleep(SLEEP_DELAY)
    led.value = False
###
