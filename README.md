# MidiMakers

### This is a [collection][wiki] of small [midi][] controller projects.

*  [TrinketCC](https://gitlab.com/inventhouse/midimakers/wikis/trinket-cc) - A small [continuous controller][CC] device built around [Adafruit's Trinket M0][trinket] low-cost CircuitPython board
![TrinketCC-small](https://gitlab.com/inventhouse/midimakers/wikis/uploads/7af44aca44c9dd50dccd2e03a121eb3f/TrinketCC-small.jpg)


[wiki]: https://gitlab.com/inventhouse/midimakers/wikis/home
[midi]: https://en.wikipedia.org/wiki/MIDI
[CC]: https://en.wikipedia.org/wiki/MIDI#Instrument_control
[trinket]: https://www.adafruit.com/product/3500
